(ns aoc-2017.dance-test
  (:require [aoc-2017.dance :refer :all]
            [clojure.test :refer :all]))

(deftest execute-dance-move-test
  (testing "can spin"
    (is (= (vec "cdeab")
           (execute-dance-move "abcde" [:spin 3]))))
  (testing "can exchange"
    (is (= (vec "cdeba")
           (execute-dance-move (vec "cdeab") [:exchange 3 4]))))
  (testing "can partner"
    (is (= (vec "dceba")
           (execute-dance-move (vec "cdeba") [:partner \c \d])))))

(deftest dance-test
  (let [move-strs ["x6/13" "pk/n" "x9/2" "pl/o" "x12/1" "pm/n" "s10" "x13/7"
                   "s7" "x1/11" "s9" "x13/10" "pa/d" "x5/9" "s12" "x10/4" "s5"]]
    (testing "single dance"
      (is (= "fkhicdpbaeognjml" (dance move-strs))))
    (testing "long dance"
      (is (= "acjdkfgeibmlnhop" (dance move-strs 10))))
    (testing "repeating dance"
      ;; For above moves repetition occurs after 30 dances.
      (is (= "acjdkfgeibmlnhop" (dance move-strs 1000))))))
