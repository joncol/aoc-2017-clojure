(ns aoc-2017.union-find-test
  (:require [aoc-2017.union-find :refer :all]
            [clojure.test :refer :all]))

(deftest union-test
  (let [uf (union-find 4)]
    (testing "can connect two elements"
      (is (= [0 1 3 3]
             (:parents (union uf 2 3)))))
    (testing "moves the smaller tree"
      (is (= [0 3 3 3]
             (:parents (-> uf
                           (union 2 3)
                           (union 2 1))))))))
