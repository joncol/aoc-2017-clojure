(ns aoc-2017.towers-test
  (:require [aoc-2017.towers :refer :all]
            [clojure.test :refer :all]
            [clojure.zip :as z]))

(def ^:private data
  ["pbga (66)"
   "xhth (57)"
   "ebii (61)"
   "havc (66)"
   "ktlj (57)"
   "fwft (72) -> ktlj, cntj, xhth"
   "qoyq (66)"
   "padx (45) -> pbga, havc, qoyq"
   "tknk (41) -> ugml, padx, fwft"
   "jptl (61)"
   "ugml (68) -> gyxo, ebii, jptl"
   "gyxo (61)"
   "cntj (57)"])

(def ^:private tower (tower-map data))

(deftest towers-test
  (testing "can get root"
    (is (= "tknk" (tower-root tower)))))

(deftest total-weight-test
  (testing "weight of whole tree"
    (is (= 778 (:total-weight (z/node (tower-zipper tower)))))))

(deftest fix-weight-test
  (testing "can fix weight of example tree"
    (is (= 60 (fix-weight (tower-zipper tower))))))
