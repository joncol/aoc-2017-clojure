(ns aoc-2017.scanner-test
  (:require [aoc-2017.scanner :refer :all]
            [clojure.test :refer :all]))

(def ^:private scanner-lines ["0: 3"
                              "1: 2"
                              "4: 4"
                              "6: 4"])

(deftest trip-severity-test
  (testing "gives the correct trip severity for the given example"
    (is (= 24 (trip-severity scanner-lines)))))

(deftest trip-caught-delay-test
  (testing "gives the correct result for the given example"
    (is (= 10 (trip-caught-delay scanner-lines)))))
