(ns aoc-2017.tubes-test
  (:require [aoc-2017.tubes :refer :all]
            [clojure.test :refer :all]))

(def ^:private maze
  ["    |          "
   "    |  +--+    "
   "    A  |  C    "
   "F---|----E|--+ "
   "    |  |  |  D "
   "    +B-+  +--+ "])

(deftest new-dir-test
  (testing "finds new direction at first turn"
    (is (= 0 (new-dir maze 4 5 1)))))

(deftest tubes-test
  (testing "collects characters in order"
    (is (= ["ABCDEF" 38] (tubes maze)))))
