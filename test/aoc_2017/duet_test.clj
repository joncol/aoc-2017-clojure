(ns aoc-2017.duet-test
  (:require [aoc-2017.duet :refer :all]
            [clojure.test :refer :all]))

(def ^:private instr-strs
  ["set a 1"
   "add a 2"
   "mul a a"
   "mod a 5"
   "snd a"
   "set a 0"
   "rcv a"
   "jgz a -1"
   "set a 1"
   "jgz a -2"])

(defn exec-with-state [instr-str state]
  (->> instr-str
       parse-instruction
       (execute state)))

(deftest execute-test
  (testing "snd updates frequency"
    (let [state (exec-with-state "snd a" {:regs {:a 440}})]
      (is (= 440 (get state :snd-freq)))))
  (testing "set updates register"
    (let [state (exec-with-state "set a 100" nil)]
      (is (= 100 (get-in state [:regs :a])))))
  (testing "add modifies register"
    (let [state (exec-with-state "add a 20" {:regs {:a 440}})]
      (is (= 460 (get-in state [:regs :a])))))
  (testing "mul modifies register"
    (let [state (exec-with-state "mul a -2" {:regs {:a 440}})]
      (is (= -880 (get-in state [:regs :a])))))
  (testing "mod modifies register"
    (let [state (exec-with-state "mod a 3" {:regs {:a 10}})]
      (is (= 1 (get-in state [:regs :a])))))
  (testing "rcv updates the rcv frequency if value of register is non-zero"
    (let [state (exec-with-state "rcv a" {:regs {:a 1} :snd-freq 440})]
      (is (= 440 (get state :rcv-freq)))))
  (testing "rcv does nothing if value of register is zero"
    (let [state (exec-with-state "rcv a" {:regs {:a 0} :snd-freq 440})]
      (is (= nil (get state :rcv-freq)))))
  (testing "jgz sets jump if register is greater than zero"
    (let [state (exec-with-state "jgz a -10" {:regs {:a 1}})]
      (is (= -10 (get state :jump)))))
  (testing "jgz does nothing if register is zero"
    (let [state (exec-with-state "jgz a -10" {:regs {:a 0}})]
      (is (= nil (get state :jump))))))

(deftest duet-test
  (testing "gives correct result for example code"
    (is (= 4 (duet instr-strs)))))

(deftest duet*-test
  (testing "sends from one program to the other"
    (is (= 1 (duet* ["set a 1" "snd a" "rcv b"]))))
  (testing "detects deadlocks"
    (is (= 0 (duet* ["rcv a"]))))
  (testing "gives correct result for simple example"
    (is (= 3 (duet* ["snd 1"
                     "snd 2"
                     "snd p"
                     "rcv a"
                     "rcv b"
                     "rcv c"
                     "rcv d"]))))
  (testing "gives correct result for example code"
    (is (= 1 (duet* instr-strs)))))
