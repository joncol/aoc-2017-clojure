(ns aoc-2017.passphrase-test
  (:require  [aoc-2017.passphrase :refer :all]
             [clojure.test :refer :all]))

(deftest passphrase-valid?-test
  (testing "valid passphrase"
    (is (passphrase-valid? "aa bb cc dd ee")))
  (testing "invalid passphrase"
    (is (not (passphrase-valid? "aa bb cc dd aa"))))
  (testing "another valid passphrase"
    (is (passphrase-valid? "aa bb cc dd aaa"))))

(deftest anagram-passphrase-valid?-test
  (testing "valid passphrase"
    (is (anagram-passphrase-valid? "abcde fghij")))
  (testing "invalid passphrase"
    (is (not (anagram-passphrase-valid? "abcde xyz ecdab"))))
  (testing "another valid passphrase"
    (is (anagram-passphrase-valid? "a ab abc abd abf abj")))
  (testing "yet another valid passphrase"
    (is (anagram-passphrase-valid? "iiii oiii ooii oooi oooo")))
  (testing "another invalid passphrase"
    (is (not (anagram-passphrase-valid? "oiii ioii iioi iiio")))))
