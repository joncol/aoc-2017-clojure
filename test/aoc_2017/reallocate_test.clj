(ns aoc-2017.reallocate-test
  (:require [aoc-2017.reallocate :refer :all]
            [clojure.test :refer :all]))

(deftest reallocate-test
  (testing "first reallocation"
    (is (= [2 4 1 2] (reallocate [0 2 7 0]))))
  (testing "second reallocation"
    (is (= [3 1 2 3] (reallocate [2 4 1 2]))))
  (testing "third reallocation"
    (is (= [0 2 3 4] (reallocate [3 1 2 3]))))
  (testing "fourth reallocation"
    (is (= [1 3 4 1] (reallocate [0 2 3 4]))))
  (testing "fifth reallocation"
    (is (= [2 4 1 2] (reallocate [1 3 4 1])))))

(deftest reallocate-until-repetition-test
  (testing "returns number of reallocations before repetition"
    (is (= 5 (first (reallocate-until-repetition [0 2 7 0]))))))
