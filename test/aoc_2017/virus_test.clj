(ns aoc-2017.virus-test
  (:require [aoc-2017.virus :refer :all]
            [clojure.test :refer :all]))

(def ^:private grid-lines ["..#"
                           "#.."
                           "..."])

(deftest infection-count-test
  (testing "gives correct results for example grid"
    (is (= 1 (infection-count grid-lines 1)))
    (is (= 5 (infection-count grid-lines 7)))
    (is (= 41 (infection-count grid-lines 70)))
    (is (= 5587 (infection-count grid-lines 10000)))))

(deftest infection-count*-test
  (testing "gives correct results for example grid"
    (is (= 26 (infection-count* grid-lines 100)))
    #_(is (= 2511944 (infection-count* grid-lines (long 10e6))))))
