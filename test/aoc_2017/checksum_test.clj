(ns aoc-2017.checksum-test
  (:require [aoc-2017.checksum :refer :all]
            [clojure.test :refer :all]))

(deftest checksum-test
  (testing "simple table, min-max test"
    (is (= 18 (checksum "5\t1\t9\t5\n7\t5\t3\n2\t4\t6\t8\n"
                        row-min-max))))
  (testing "simple table, quotient test"
    (is (= 9 (checksum "5\t9\t2\t8\n9\t4\t7\t3\n3\t8\t6\t5\n"
                       row-quotient)))))
