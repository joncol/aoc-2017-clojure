(ns aoc-2017.knot-hash-test
  (:require [aoc-2017.knot-hash :refer :all]
            [clojure.test :refer :all]))

(deftest reverse-subarray!-test
  (testing "reverses whole list"
    (is (= [3 2 1] (vec (reverse-subarray! (int-array [1 2 3]) 0 3)))))
  (testing "reverses simple sublist"
    (is (= [1 3 2] (vec (reverse-subarray! (int-array [1 2 3]) 1 2)))))
  (testing "reverses wrapping sublist"
    (is (= [5 4 3 2 1] (vec (reverse-subarray! (int-array [1 2 3 4 5]) 3 4)))))
  (testing "reverses wrapping list"
    (is (= [1 5 4 3 2] (vec (reverse-subarray! (int-array [1 2 3 4 5]) 3 5)))))
  (testing "works for singleton sublist"
    (is (= [1 2 3 4 5] (vec (reverse-subarray! (int-array [1 2 3 4 5]) 2 1))))))

(deftest knot-hash1-test
  (testing "it works for example list"
    (is (= 12 (knot-hash1 5 [3 4 1 5])))))

(deftest knot-hash2-test
  (testing "empty string"
    (is (= "a2582a3a0e66e6e86e3812dcb672a272" (hash->string (knot-hash2 "")))))
  (testing "example string 1"
    (is (= "33efeb34ea91902bb2f59c9920caa6cd"
           (hash->string (knot-hash2 "AoC 2017")))))
  (testing "example string 2"
    (is (= "3efbe78a8d82f29979031a4aa0b16a9d"
           (hash->string (knot-hash2 "1,2,3")))))
  (testing "example string 3"
    (is (= "63960835bcdc130f0b66d7ff4f6a5a8e"
           (hash->string (knot-hash2 "1,2,4"))))))
