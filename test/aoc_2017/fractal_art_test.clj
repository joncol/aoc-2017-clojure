(ns aoc-2017.fractal-art-test
  (:require [aoc-2017.fractal-art :refer :all]
            [clojure.test :refer :all]))

(def ^:private rules
  ["../.# => ##./#../..."
   ".#./..#/### => #..#/..../..../#..#"])

(deftest get-pattern-cell-test
  (testing "returns part of pattern"
    (is (= [".." ".#"]
           (get-pattern-cell ["#..#" "...." "...." "#..#"] 2 1 1)))))

(deftest subdivide-pattern-test
  (testing "does not divide 3x3 pattern"
    (is (= [[[".#." "..#" "###"]]]
           (subdivide-pattern [".#." "..#" "###"]))))
  (testing "subdivides 4x4 pattern"
    (is (= [[["#." ".."] [".#" ".."]] [[".." "#."] [".." ".#"]]]
           (subdivide-pattern ["#..#" "...." "...." "#..#"])))))

(deftest join-patterns-test
  (testing "joins smaller patterns into large pattern"
    (is (= ["#..#" "...." "...." "#..#"]
           (join-patterns [[["#." ".."] [".#" ".."]]
                           [[".." "#."] [".." ".#"]]])))))

(deftest pixel-count-test
  (testing "counts the number of set pixels in pattern"
    (is (= 5 (pixel-count [".#." "..#" "###"])) )))

(deftest evolve-pattern-test
  (testing "works for example rules"
    (is (= 12 (pixel-count (evolve-pattern 2 rules))))))
