(ns aoc-2017.particles-test
  (:require [aoc-2017.particles :refer :all]
            [clojure.test :refer :all]))

(def ^:private particle-data1
  ["p=<2366,784,-597>, v=<-12,-41,50>, a=<-5,1,-2>"
   "p=<-2926,-3402,-2809>, v=<-55,65,-16>, a=<11,4,8>"
   "p=<2290,257,-3040>, v=<41,119,57>, a=<-10,-10,5>"
   "p=<3090,-493,-1340>, v=<-134,19,-37>, a=<1,0,7>"
   "p=<-1338,2295,4101>, v=<-158,34,-48>, a=<19,-12,-12>"
   "p=<-1140,1657,-981>, v=<17,-75,45>, a=<3,0,0>"
   "p=<1544,40,141>, v=<-59,33,-75>, a=<-1,-3,6>"])

(def ^:private particle-data2
  ["p=<-6,0,0>, v=<3,0,0>, a=<0,0,0>"
   "p=<-4,0,0>, v=<2,0,0>, a=<0,0,0>"
   "p=<-2,0,0>, v=<1,0,0>, a=<0,0,0>"
   "p=<3,0,0>, v=<-1,0,0>, a=<0,0,0>"])

(def ^:private particle-data3
  ["p=<-1405,-706,1623>, v=<13,78,-74>, a=<13,-3,-7>"
   "p=<-633,-2898,1623>, v=<-9,-49,109>, a=<2,10,-11>"])

(deftest particles-test
  (testing "gets the index of the particle with the smallest acceleration"
    (is (= 5 (lowest-acc particle-data1)))))

(deftest non-colliding-count-test
  (testing "removes colliding particles"
    (is (= 1 (non-colliding-count particle-data2)))
    (is (= 2 (non-colliding-count particle-data3)))))
