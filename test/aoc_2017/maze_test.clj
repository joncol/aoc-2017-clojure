(ns aoc-2017.maze-test
  (:require [aoc-2017.maze :refer :all]
            [clojure.test :refer :all]))

(deftest maze-test
  (testing "example maze"
    (is (= 5 (maze inc [0 3 0 1 -3])))))

(deftest maze2-test
  (testing "other update-fn maze"
    (is (= 10 (maze update-fn-b [0 3 0 1 -3])))))
