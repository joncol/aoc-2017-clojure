(ns aoc-2017.spiral-test
  (:require [aoc-2017.spiral :refer :all]
            [clojure.test :refer :all]))

(deftest spiral-test
  #_(testing "n = 1"
    (is (= 0 (spiral 1))))
  (testing "n = 2"
    (is (= 1 (spiral 2))))
  (testing "n = 3"
    (is (= 2 (spiral 3))))
  (testing "n = 4"
    (is (= 1 (spiral 4))))
  (testing "n = 5"
    (is (= 2 (spiral 5))))
  (testing "n = 6"
    (is (= 1 (spiral 6))))
  (testing "n = 7"
    (is (= 2 (spiral 7))))
  (testing "n = 8"
    (is (= 1 (spiral 8))))
  (testing "n = 9"
    (is (= 2 (spiral 9))))
  (testing "n = 10"
    (is (= 3 (spiral 10))))
  (testing "n = 11"
    (is (= 2 (spiral 11))))
  (testing "n = 12"
    (is (= 3 (spiral 12))))
  (testing "n = 23"
    (is (= 2 (spiral 23))))
  (testing "n = 1024"
    (is (= 31 (spiral 1024)))))
