(ns aoc-2017.stream-processing-test
  (:require [aoc-2017.stream-processing :refer :all]
            [clojure.test :refer :all]))

(deftest score-test
  (testing "score 1"
    (is (= 1 (score "{}"))))
  (testing "score 2"
    (is (= 6 (score "{{{}}}"))))
  (testing "score 3"
    (is (= 5 (score "{{},{}}"))))
  (testing "score 4"
    (is (= 16 (score "{{{},{},{{}}}}"))))
  (testing "score 5"
    (is (= 1 (score "{<{},{},{{}}>}"))))
  (testing "score 6"
    (is (= 1 (score "{<a>,<a>,<a>,<a>}"))))
  (testing "score 7"
    (is (= 1 (score "{<a>,<a>,<a>,<a>}"))))
  (testing "score 8"
    (is (= 9 (score "{{<ab>},{<ab>},{<ab>},{<ab>}}"))))
  (testing "score 9"
    (is (= 9 (score "{{<!!>},{<!!>},{<!!>},{<!!>}}"))))
  (testing "score 10"
    (is (= 3 (score "{{<a!>},{<a!>},{<a!>},{<ab>}}"))))
  (testing "score 11"
    (is (= 8 (score "{{},{{<yoyo>},<}>}}"))))
  (testing "score 12"
    (is (= 17 (score "{{{{},{}},{}}}"))))
  (testing "score 12"
    (is (= 10 (score "{{{{},<{}>},<{}>}}")))))

(deftest garbage-count-test
  (testing "simple garbage"
    (is (= 10 (garbage-count "<{o\"i!a,<{i<a>"))))
  (testing "grouped garbage"
    (is (= 10 (garbage-count "{<hello>,{{<world>}}}")))))
