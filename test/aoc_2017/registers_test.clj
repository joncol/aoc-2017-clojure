(ns aoc-2017.registers-test
  (:require [aoc-2017.registers :refer :all]
            [clojure.test :refer :all]))

(def commands ["b inc 5 if a > 1"
               "a inc 1 if b < 5"
               "c dec -10 if a >= 1"
               "c inc -20 if c == 10"])

(deftest registers-test
  (testing "simple example"
    (is (= [{"a" 1
             "c" -10}
            10]
           (run-commands commands)))))
