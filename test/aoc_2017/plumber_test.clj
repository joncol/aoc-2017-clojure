(ns aoc-2017.plumber-test
  (:require [aoc-2017.plumber :refer :all]
            [aoc-2017.union-find :refer :all]
            [clojure.test :refer :all]))

(def data ["0 <-> 2"
           "1 <-> 1"
           "2 <-> 0, 3, 4"
           "3 <-> 2, 4"
           "4 <-> 2, 3, 6"
           "5 <-> 6"
           "6 <-> 4, 5"])

(deftest plumber-test
  (testing "works for given example"
    (is (= 1 (size (plumber data) 1)))
    (is (= 6 (size (plumber data) 0)))))
