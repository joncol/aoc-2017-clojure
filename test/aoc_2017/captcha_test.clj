(ns aoc-2017.captcha-test
  (:require [aoc-2017.captcha :refer :all]
            [clojure.test :refer :all]))

(deftest captcha-test
  (testing "simple captcha"
    (is (= 3 (captcha "1122"))))
  (testing "trims whitespace"
    (is (= 3 (captcha "  1122\n"))))
  (testing "long sequence"
    (is (= 4 (captcha "1111"))))
  (testing "no sequence"
    (is (= 0 (captcha "1234"))))
  (testing "wrapping sequence"
    (is (= 9 (captcha "91212129")))))

(deftest captcha2-test
  (testing "simple captcha"
    (is (= 6 (captcha "1212" 2))))
  (testing "zero captcha"
    (is (= 0 (captcha "1221" 2))))
  (testing "single match"
    (is (= 4 (captcha "123425" 3))))
  (testing "all match"
    (is (= 12 (captcha "123123" 3))))
  (testing "ones match"
    (is (= 4 (captcha "12131415" 4)))))
