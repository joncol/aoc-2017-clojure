(ns aoc-2017.bridge-test
  (:require [aoc-2017.bridge :refer :all]
            [clojure.test :refer :all]))

(def ^:private component-lines
  ["0/2"
   "2/2"
   "2/3"
   "3/4"
   "3/5"
   "0/1"
   "10/1"
   "9/10"])

(deftest all-bridges-test
  (testing "returns the correct number of bridges"
    (let [components (mapv parse-component component-lines)]
      (is (= 2 (count (all-bridges (set components) 0)))))))

(deftest bridge-max-strength-test
  (testing "returns the max total bridge strength on a path from root to a leaf"
    (is (= 19 (bridge-max-strength '([0 2]
                                     ([2 2] ([2 3] ([3 4]) ([3 5])))
                                     ([2 3] ([3 4]) ([3 5]))))))))

(deftest strongest-bridge-test
  (testing "returns the strength of the strongest bridge using the components"
    (is (= 31 (strongest-bridge component-lines)))))

(deftest flatten-bridge-test
  (testing "returns a sequence of component sequences."
    (is (= [[[0 2] [2 2] [2 3] [3 4]]
            [[0 2] [2 2] [2 3] [3 5]]
            [[0 2] [2 3] [3 4]]
            [[0 2] [2 3] [3 5]]]
           (flatten-bridge '([0 2]
                             ([2 2] ([2 3] ([3 4]) ([3 5])))
                             ([2 3] ([3 4]) ([3 5]))))))))

(deftest longest-bridge-test
  (testing "returns the strength of the longest bridge using the components"
    (is (= 19 (longest-bridge component-lines)))))
