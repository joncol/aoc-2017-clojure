(ns aoc-2017.spinlock-test
  (:require [aoc-2017.spinlock :refer :all]
            [clojure.test :refer :all]))

(deftest spinlock1-test
  (testing "it gives correct result for example"
    (is (= 638 (spinlock1 3)))))
