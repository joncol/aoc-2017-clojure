(defproject aoc-2017 "0.1.0-SNAPSHOT"
  :description "Advent of Code 2017"
  :dependencies [[instaparse "1.4.8"]
                 [org.clojure/clojure "1.9.0"]]
  :main ^:skip-aot aoc-2017.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
