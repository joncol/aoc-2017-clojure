#include <stdio.h>

/**
 * C version of the assembly code in input23. Removed one nested loop to make it
 * faster.
 *
 * The program counts the numbers in the interval [107900, 124900] (with a step
 * size of 17) that are non-prime.
 */
int main()
{
    int h = 0;
    for (int b = 107900; b <= 124900; b += 17) {
        int f = 1;
        for (int d = 2; d < b; ++d) {
            if ((b % d) == 0) {
                f = 0;
                break;
            }
        }

        if (f == 0) {
            ++h;
        }
    }
    printf("h: %d\n", h);
}
