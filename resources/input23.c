#include <stdio.h>

int main()
{
    int h = 0;
    for (int b = 107900; b != 124900; b += 17) {
        int f = 1;
        for (int d = 2; d < b; ++d) {
            for (int e = 2; e < b; ++e) {
                if (d * e == b) {
                    f = 0;
                    break;
                }
            }
            if (!f) {
                break;
            }
        }

        if (f == 0) {
            ++h;
        }
    }
    printf("h: %d\n", h);
}
