(ns aoc-2017.core
  (:require [aoc-2017.bridge :refer :all]
            [aoc-2017.captcha :refer [captcha]]
            [aoc-2017.checksum :refer :all]
            [aoc-2017.conflagration :refer :all]
            [aoc-2017.dance :refer [dance]]
            [aoc-2017.defrag :refer :all]
            [aoc-2017.duet :refer [duet duet*]]
            [aoc-2017.fractal-art :refer :all]
            [aoc-2017.generators :refer :all]
            [aoc-2017.hex-grid :refer :all]
            [aoc-2017.knot-hash :refer :all]
            [aoc-2017.maze :refer :all]
            [aoc-2017.particles :refer :all]
            [aoc-2017.passphrase :refer :all]
            [aoc-2017.plumber :refer [plumber]]
            [aoc-2017.reallocate :refer [reallocate-until-repetition]]
            [aoc-2017.registers :refer [run-commands]]
            [aoc-2017.scanner :refer :all]
            [aoc-2017.spinlock :refer :all]
            [aoc-2017.spiral :refer :all]
            [aoc-2017.stream-processing :refer :all]
            [aoc-2017.towers :refer :all]
            [aoc-2017.tubes :refer [tubes]]
            [aoc-2017.turing :refer :all]
            [aoc-2017.union-find :refer :all]
            [aoc-2017.virus :refer :all]
            [clojure.java.io :as io]
            [clojure.string :as str])
  (:gen-class))

(defn- parse-int [s]
  (let [digits (apply str (take-while #(or (Character/isDigit %)
                                           (get (into #{} "-+") %)) s))]
    (Integer/parseInt digits)))

(defn- lines [s]
  (str/split s #"\n"))

(defn- words [s]
  (str/split s #"\s"))

(defn- elems [s]
  (str/split (str/trim s) #","))

(defn- solve [arg data]
  (case arg
    "1a"  (captcha data)
    "1b"  (captcha data (/ (count data) 2))
    "2a"  (checksum data row-min-max)
    "2b"  (checksum data row-quotient)
    "3a"  (spiral (parse-int data))
    "3b"  (spiral-sums (parse-int data))
    "4a"  (->> (lines data)
               (filter passphrase-valid?)
               count)
    "4b"  (->> (lines data)
               (filter anagram-passphrase-valid?)
               count)
    "5a"  (->> (lines data)
               (map parse-int)
               (maze inc))
    "5b"  (->> (lines data)
               (map parse-int)
               (maze update-fn-b))
    "6a"  (->> (words data)
               (map parse-int)
               reallocate-until-repetition
               first)
    "6b"  (->> (words data)
               (map parse-int)
               reallocate-until-repetition
               second)
    "7a"  (->> (lines data)
               tower-map
               tower-root)
    "7b"  (->> (lines data)
               tower-map
               tower-zipper
               fix-weight)
    "8a"  (->> (lines data)
               run-commands
               first
               (apply max-key val)
               val)
    "8b"  (->> (lines data)
               run-commands
               second)
    "9a"  (score (str/trim data))
    "9b"  (garbage-count (str/trim data))
    "10a" (knot-hash1 256 (map #(Integer/parseInt %) (elems data)))
    "10b" (hash->string (knot-hash2 data))
    "11a" (->> (elems data)
               (map keyword)
               walk-hex-grid
               last
               (hex-distance [0 0 0]))
    "11b" (->> (elems data)
               (map keyword)
               walk-hex-grid
               (apply max-key #(hex-distance % [0 0 0]))
               (hex-distance [0 0 0]))
    "12a" (size (plumber (lines data)) 0)
    "12b" (connected-group-count (plumber (lines data)))
    "13a" (trip-severity (lines data))
    "13b" (trip-caught-delay (lines data))
    "14a" (used-count (create-field data))
    "14b" (group-count (create-field data))
    "15a" (match-gen-count data)
    "15b" (match-gen-count2 data)
    "16a" (dance (elems data))
    "16b" (dance (elems data) (long 1e9))
    "17a" (spinlock1 (Integer/parseInt (str/trim data)))
    "17b" (spinlock2 (Integer/parseInt (str/trim data)))
    "18a" (duet (lines data))
    "18b" (duet* (lines data))
    "19a" (first (tubes (lines data)))
    "19b" (second (tubes (lines data)))
    "20a" (lowest-acc (lines data))
    "20b" (non-colliding-count (lines data))
    "21a" (pixel-count (evolve-pattern 5 (lines data)))
    "21b" (pixel-count (evolve-pattern 18 (lines data)))
    "22a" (infection-count (lines data) 10000)
    "22b" (infection-count* (lines data) (long 10e6))
    "23a" (mul-count (lines data))
    "23b" 907 ; Converted assembly code to C program.
    "24a" (strongest-bridge (lines data))
    "24b" (longest-bridge (lines data))
    "25"  (run-turing data)
    ))

(defn -main [& args]
  (let [arg      (first args)
        day      (parse-int arg)
        filename (str "resources/input" day)]
    (if (.exists (io/as-file filename))
      (println "result:" (solve arg (slurp filename)))
      (println "not implemented yet :)"))))

(defn run-all []
  (let [problem-names (concat (for [n (range 1 25), l "ab"] (str n l)) ["25"])]
    (doseq [problem-name problem-names]
      (print (str "Running " problem-name "..."))
      (flush)
      (time (-main problem-name))
      (println))))
