(ns aoc-2017.registers
  (:require [instaparse.core :as insta]))

(def ^:private command-parser
  (insta/parser "command = reg <ws> instr <ws> arg <ws>
                           <'if'> <ws> reg <ws> cond <ws> arg
                 ws      = #'\\s+'
                 reg     = #'\\w+'
                 instr   = 'inc' | 'dec'
                 cond    = '==' | '!=' | '<' | '>' | '<=' | '>='
                 arg     = #'-?\\d+'"))

(def instr-fns {"inc" +
                "dec" -})

(def cond-fns {"==" =
               "!=" not=
               "<"  <
               ">"  >
               "<=" <=
               ">=" >=})

(defn- run-command [[registers max-val] command-str]
  (let [[_ [_ reg] [_ instr] [_ instr-arg]
         [_ cond-reg] [_ cond-op] [_ cond-arg]]
        (insta/parse command-parser command-str)
        instr-arg   (Integer/parseInt instr-arg)
        cond-arg    (Integer/parseInt cond-arg)
        instr-fn    (get instr-fns instr)
        cond-fn     (get cond-fns cond-op)
        old-reg-val (get registers reg 0)]
    (if (cond-fn (get registers cond-reg 0) cond-arg)
      (let [new-val (instr-fn old-reg-val instr-arg)]
        [(assoc registers reg new-val) (max max-val new-val)])
      [registers max-val])))

(defn run-commands [command-strs]
  (reduce run-command [{} 0] command-strs))
