(ns aoc-2017.particles
  (:require [aoc-2017.util :refer [solve-int-quadratic]]
            [instaparse.core :as insta]
            [clojure.set :as set]))

(def ^:private particle-parser
  (insta/parser "<particle> = <'p='> pos <', v='> vel <', a='> acc
                 pos        = vector
                 vel        = vector
                 acc        = vector
                 <vector>   = <'<'> number <','> number <','> number <'>'>
                 <number>   = #'-?\\d+'"))

(defn- transform-vector [kw]
  (fn [x y z]
    [kw (map #(Integer/parseInt %) [x y z])]))

(defn parse-particle [s]
  (into {} (insta/transform
            {:pos (transform-vector :pos)
             :vel (transform-vector :vel)
             :acc (transform-vector :acc)}
            (particle-parser s))))

(defn manhattan-norm [[x y z]]
  (+ (Math/abs x)
     (Math/abs y)
     (Math/abs z)))

(defn lowest-acc [particle-strs]
  (->> particle-strs
       (map parse-particle)
       (map-indexed vector)
       (apply min-key (comp manhattan-norm :acc second))
       first))

(defn- solve-eq [p0 p1 v0 v1 a0 a1]
  (let [a (- a0 a1)
        b (- (+ (* 2 (- v0 v1)) a0) a1)
        c (* 2 (- p0 p1))]
    (solve-int-quadratic a b c)))

(defn- collision-times [particles combinations]
  (reduce (fn [coll-times [i j]]
            (let [pi            (get particles i)
                  pj            (get particles j)
                  [px0 py0 pz0] (:pos pi)
                  [vx0 vy0 vz0] (:vel pi)
                  [ax0 ay0 az0] (:acc pi)
                  [px1 py1 pz1] (:pos pj)
                  [vx1 vy1 vz1] (:vel pj)
                  [ax1 ay1 az1] (:acc pj)
                  times         (->> [(solve-eq px0 px1 vx0 vx1 ax0 ax1)
                                      (solve-eq py0 py1 vy0 vy1 ay0 ay1)
                                      (solve-eq pz0 pz1 vz0 vz1 az0 az1)]
                                     (filter (comp not true?))
                                     (map set)
                                     (apply set/intersection)
                                     (filter (comp not neg?)))]
              (merge-with set/union coll-times (zipmap times (repeat #{i j})))))
          {} combinations))

(defn- collision-count [coll-times]
  (->> coll-times
       keys
       sort
       (reduce (fn [[coll-times removed] time]
                 (let [particles   (get coll-times time)
                       coll-times* (update coll-times time
                                           #(set/difference % removed))
                       removed*    (set/union removed particles)]
                   [coll-times* removed*]))
               [coll-times #{}])
       first
       (filterv (fn [[k v]] (>= (count v) 2)))
       (mapv (fn [[k v]] (count v)))
       (reduce +)))

(defn non-colliding-count [particle-strs]
  (let [particles    (mapv parse-particle particle-strs)
        cnt          (count particles)
        combinations (for [i (range (dec cnt)), j (range (inc i) cnt)] [i j])
        coll-times   (collision-times particles combinations)
        coll-count   (collision-count coll-times)]
    (- cnt coll-count)))
