(ns aoc-2017.checksum
  (:require [clojure.string :as str]))

(defn read-table [s]
  (->> (str/split-lines s)
       (map #(str/split % #"\s"))
       (map (fn [row] (map #(Integer/parseInt %) row)))))

(defn row-min-max [sum row]
  (let [largest  (apply max row)
        smallest (apply min row)]
    (+ sum (- largest smallest))))

(defn row-quotient [sum row]
  (let [quot-rem (juxt quot rem)]
    (->> (for [i (range (count row)), j (range (count row)) :when (not= i j)]
           (quot-rem (nth row i) (nth row j)))
         (filter (fn [[q r]] (zero? r)))
         ffirst
         (+ sum))))

(defn checksum [table-str row-fn]
  (let [table (read-table table-str)]
    (reduce row-fn 0 table)))
