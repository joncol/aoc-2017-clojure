(ns aoc-2017.passphrase
  (:require [clojure.string :as str]))

(defn passphrase-valid?
  ([phrase] (passphrase-valid? phrase identity))
  ([phrase group-by-fn]
   (->> (str/split phrase #" ")
        (group-by group-by-fn)
        vals
        (every? #(= 1 (count %))))))

(defn anagram-passphrase-valid? [phrase]
  (passphrase-valid? phrase sort))
