(ns aoc-2017.union-find)

(defprotocol IUnionFind
  (parent [this x])
  (size [this x])
  (root [this x])
  (union [this x y])
  (connected-group-count [this]))

(defrecord UnionFind [parents sizes]
  IUnionFind
  (parent [this x]
    (nth parents x))
  (size [this x]
    (nth sizes (root this x)))
  (root [this x]
    (loop [node x]
      (let [p (parent this node)]
        (if (= p node)
          node
          (recur p)))))
  (union [this x y]
    (let [x-root (root this x)
          y-root (root this y)
          x-size (nth sizes x-root)
          y-size (nth sizes y-root)]
      (cond
        (= x-root y-root)  this
        (<= x-size y-size) (new UnionFind (assoc parents x-root y-root)
                                (update sizes y-root #(+ x-size %)))
        :else              (new UnionFind (assoc parents y-root x-root)
                                (update sizes x-root #(+ y-size %))))))
  (connected-group-count [this]
    (->> this
         :parents
         (map #(root this %))
         (group-by identity)
         count)))

(defn union-find [n]
  (new UnionFind (vec (range n)) (vec (repeat n 1))))
