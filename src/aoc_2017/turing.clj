(ns aoc-2017.turing
  (:require [instaparse.core :as insta]))

(def turing-parser
  (insta/parser "<turing>      = header states
                 <header>      = <'Begin in state '> init-state
                                 <'.\\n'> step-count
                 init-state    = state-name
                 step-count    = <'Perform a diagnostic checksum after '>
                                 number <' steps.\\n'>
                 states        = state+
                 <state>       = <'\nIn state '> state-name <':\\n'> state-rules
                 state-rules   = rules+
                 rules         = <'  If the current value is '> number <':\\n'>
                                 <'    - Write the value '> number <'.\\n'>
                                 <'    - Move one slot to the '> dir <'.\\n'>
                                 <'    - Continue with state '> state-name
                                 <'.\\n'>
                 <state-name>  = #'\\w'
                 dir           = 'left' | 'right'
                 number        = #'\\d+'"))

(defn- parse-turing [data]
  (into {} (insta/transform
            {:number      #(Integer/parseInt %)
             :states      (fn [& xs]
                            [:states (into {} (map vec (partition 2 xs)))])
             :state-rules (fn [& xs] (apply vector xs))
             :rules       (fn [in-val out-val dir next-state]
                            {:in-val     in-val
                             :out-val    out-val
                             :dir        dir
                             :next-state next-state})
             :dir         keyword}
            (turing-parser data))))

(defn move [pos dir]
  (case dir
    :left  (dec pos)
    :right (inc pos)))

(defn run-turing [data]
  (let [machine (parse-turing data)]
    (loop [i     0
           pos   0
           tape  {}
           state (:init-state machine)]
      (if (< i (:step-count machine))
        (let [rule   (->> (get-in machine [:states state])
                          (filter #(= (get tape pos 0) (:in-val %)))
                          first)
              pos*   (move pos (:dir rule))
              state* (:next-state rule)
              tape*  (assoc tape pos (:out-val rule))]
          (recur (inc i) pos* tape* state*))
        (->> tape
             (filter (fn [[k v]] (= 1 v)))
             count)))))
