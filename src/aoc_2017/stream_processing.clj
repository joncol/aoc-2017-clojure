(ns aoc-2017.stream-processing
  (:require [instaparse.core :as insta]))

(def ^:private stream-parser
  (insta/parser "<stream>  = thing (<','> thing)* | epsilon
                 <thing>   = group | garbage
                 group     = <'{'> stream <'}'>
                 garbage   = <'<'> (#'[^>]' | ignored)* <'>'>
                 <ignored> = <#'!.'>"))

(defn- parse [stream]
  (insta/parse stream-parser stream))

(defn- score-reducer [[total-score level] x]
  (if (and (seq x) (= :group (first x)))
    (let [[child-score _] (reduce score-reducer [0 (inc level)] (next x))]
      [(+ total-score child-score level) level])
    [total-score level]))

(defn score [stream]
  (first (reduce score-reducer [0 1] (parse stream))))

(defn- garbage-reducer [total-count x]
  (cond
    (and (seq x) (= :garbage (first x)))
    (+ total-count (count (next x)))
    (and (seq x) (= :group (first x)))
    (let [child-garbage-count (reduce garbage-reducer 0 (next x))]
      (+ total-count child-garbage-count))
    :else count))

(defn garbage-count [stream]
  (reduce garbage-reducer 0 (parse stream)))
