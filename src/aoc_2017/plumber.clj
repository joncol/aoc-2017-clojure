(ns aoc-2017.plumber
  (:require [aoc-2017.union-find :refer :all]
            [instaparse.core :as insta]))

(def ^:private pipe-parser
  (insta/parser "<pipe>  = index <' <-> '> list
                 <index> = #'\\d+'
                 <list>  = (<', '>? index)+"))

(defn- parse-line [l]
  (map #(Integer/parseUnsignedInt %) (insta/parse pipe-parser l)))

(defn plumber [pipe-lines]
  (reduce (fn [uf l]
            (let [[i & conns] (parse-line l)]
              (reduce (fn [uf x] (union uf i x)) uf conns)))
          (union-find (count pipe-lines)) pipe-lines))
