(ns aoc-2017.dance
  (:require [aoc-2017.util :refer [find-values rotate]]
            [instaparse.core :as insta]))

(def ^:private dance-move-parser
  (insta/parser "<move>   = spin | exchange | partner
                 spin     = <'s'>#'\\d+'
                 exchange = <'x'>#'\\d+' <'/'> #'\\d+'
                 partner  = <'p'>#'[a-p]' <'/'> #'[a-p]'"))

(defn- parse-dance-move [s]
  (insta/transform
   {:spin     #(-> [:spin (Integer/parseInt %)])
    :exchange #(-> [:exchange (Integer/parseInt %1) (Integer/parseInt %2)])
    :partner  #(-> [:partner (first %1) (first %2)])}
   (dance-move-parser s)))

(defn execute-dance-move [programs [move-type & params]]
  (case move-type
    :spin     (let [[n] params]
                (vec (rotate (- n) programs)))
    :exchange (let [[i j] params]
                (assoc programs i (programs j), j (programs i)))
    :partner  (let [[x y] params
                    i     (first (find-values programs x))
                    j     (first (find-values programs y))]
                (assoc programs i y, j x))))

(defn- parse-moves [move-strs]
  (->> move-strs
       (map (comp first parse-dance-move))
       (group-by (fn [move] (if (#{:spin :exchange} (first move))
                              :index
                              :mutate)))))

(defn- index-order [moves]
  (->> moves
       (reduce execute-dance-move (vec "abcdefghijklmnop"))
       (apply str)
       (map #(- (int %) (int \a)))))

(defn dance
  ([move-strs]
   (->> move-strs
        (map (comp first parse-dance-move))
        (reduce execute-dance-move (vec "abcdefghijklmnop"))
        (apply str)))
  ([move-strs n]
   (let [moves   (parse-moves move-strs)
         indices (index-order (:index moves))]
     (->> (loop [chars     (vec "abcdefghijklmnop")
                 i         0
                 cache-set #{}
                 cache-vec []]
            (let [chars*  (vec (map (partial nth chars) indices))
                  chars** (reduce execute-dance-move chars* (:mutate moves))
                  s       (apply str chars**)]
              (if (get cache-set s)
                (get cache-vec (dec (mod n i)))
                (if (< (inc i) n)
                  (recur chars** (inc i) (conj cache-set s) (conj cache-vec s))
                  chars**))))
          (apply str)))))
