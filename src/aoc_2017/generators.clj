(ns aoc-2017.generators
  (:require [instaparse.core :as insta]))

(def ^:private seed-parser
  (insta/parser "<seed> = (<#'Generator . starts with '> #'\\d+'<'\n'?>)+"))

(defn- mask [x]
  (bit-and x (dec (bit-shift-left 1 16))))

(defn- parse-seeds [data]
  (vec (map #(Integer/parseInt %) (insta/parse seed-parser data))))

(defn match-gen-count ^long [data]
  (let [divisor                     ^long (dec (bit-shift-left 1 31))
        [^long mul-a ^long mul-b]   [16807 48271]
        [^long seed-a ^long seed-b] (parse-seeds data)]
    (loop [^long a seed-a
           ^long b seed-b
           c       (long 40e6)
           m       0]
      (if (pos? (unchecked-dec c))
        (recur (mod (unchecked-multiply a mul-a) divisor)
               (mod (unchecked-multiply b mul-b) divisor)
               (unchecked-dec c)
               (if (= (mask a) (mask b))
                 (unchecked-inc m)
                 m))
        m))))

(defn match-gen-count2 [data]
  (let [divisor         (dec (bit-shift-left 1 31))
        [mul-a mul-b]   [16807 48271]
        [seed-a seed-b] (parse-seeds data)
        seq-a           (->> seed-a
                             (iterate #(mod (* % mul-a) divisor))
                             (filter #(zero? (bit-and % 3))))
        seq-b           (->> seed-b
                             (iterate #(mod (* % mul-b) divisor))
                             (filter #(zero? (bit-and % 7))))
        cnt             (long 5e6)]
    (->> (map #(= (mask %1) (mask %2)) (take cnt seq-a) seq-b)
         (filter true?)
         count)))
