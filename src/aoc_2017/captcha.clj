(ns aoc-2017.captcha
  (:require [aoc-2017.util :refer [rotate]]
            [clojure.string :as str]))

(defn digits-str->list [x]
  (->> x
       str/trim
       (map #(- (int %) (int \0)))))

(defn- aux [[sum [y & ys]] x]
  (if (= x y)
    [(+ sum y) ys]
    [sum ys]))

(defn captcha
  ([digits-str]
   (captcha digits-str 1))
  ([digits-str n]
   (let [digits (digits-str->list digits-str)]
     (first (reduce aux [0 (rotate n digits)] digits)))))
