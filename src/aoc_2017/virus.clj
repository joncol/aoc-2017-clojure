(ns aoc-2017.virus)

(def ^:private directions
  [[1 0] [0 1] [-1 0] [0 -1]])

(defn parse-grid [lines]
  (->> lines
       (map-indexed (fn [y row]
                      (map-indexed (fn [x ch] [[x y] ch]) row)))
       (apply concat)
       (into {})))

(defn turn-cw [^long dir]
  (bit-and (unchecked-inc-int dir) 3))

(defn turn-ccw [^long dir]
  (bit-and (unchecked-dec-int dir) 3))

(defn move [^"[J" [x y] ^long dir]
  (let [^"[J" [xd yd] (get directions dir)]
    [(unchecked-add x xd)
     (unchecked-add y yd)]))

(defn flip-infected [grid ^long x ^long y]
  (update grid [x y] #(if (= \# %) \. \#)))

(defn update-infected [grid ^long x ^long y]
  (update grid [x y] #(case %
                        \W \#
                        \# \F
                        \F \.
                        \W)))

(defn infection-count [grid-lines n]
  (loop [grid      (parse-grid grid-lines)
         x         (quot (count (first grid-lines)) 2)
         y         (quot (count grid-lines) 2)
         dir       3
         inf-count 0
         i         0]
    (if (< i n)
      (let [infected (= \# (get grid [x y]))
            dir*     (if infected (turn-cw dir) (turn-ccw dir))
            grid*    (flip-infected grid x y)
            [x* y*]  (move [x y] dir*)]
        (recur grid* x* y* dir* (if infected inf-count (inc inf-count))
               (inc i)))
      inf-count)))

(defn infection-count* [grid-lines n]
  (loop [grid      (parse-grid grid-lines)
         x         (quot (count (first grid-lines)) 2)
         y         (quot (count grid-lines) 2)
         dir       3
         inf-count 0
         i         0]
    (if (< i n)
      (let [n        (get grid [x y])
            dir*     (case n
                       \W dir
                       \# (turn-cw dir)
                       \F (-> dir turn-cw turn-cw)
                       (turn-ccw dir))
            grid*    (update-infected grid x y)
            [x* y*]  (move [x y] dir*)]
        (recur grid* x* y* dir* (if (= n \W) (inc inf-count) inf-count)
               (inc i)))
      inf-count)))
