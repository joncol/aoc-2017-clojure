(ns aoc-2017.hex-grid)

;; Uses cube coordinates to represent hex-grid positions.

(def ^:private directions
  {:n  [0 1 -1]
   :ne [1 0 -1]
   :se [1 -1 0]
   :s  [0 -1 1]
   :sw [-1 0 1]
   :nw [-1 1 0]})

(defn hex-distance [[x1 y1 z1] [x2 y2 z2]]
  (quot (+ (Math/abs (- x2 x1))
           (Math/abs (- y2 y1))
           (Math/abs (- z2 z1)))
        2))

(defn walk-hex-grid [steps]
  (reductions (fn [acc step]
                (doall (map + acc (get directions step))))
              [0 0 0]
              steps))
