(ns aoc-2017.bridge
  (:require [clojure.set :as set]
            [clojure.walk :refer [postwalk]]
            [instaparse.core :as insta]))

(def ^:private component-parser
  (insta/parser "<component> = number <'/'> number
                 number      = #'\\d+'"))

(defn parse-component [s]
  (vec (insta/transform
        {:number #(Integer/parseInt %)}
        (component-parser s))))

(defn matching-components [components port]
  (filter (fn [[p1 p2]] ((set [p1 p2]) port)) components))

(defn- other-port [port p1 p2]
  (if (= p1 port) p2 p1))

(defn all-bridges [components port]
  (let [matches (matching-components components port)]
    (->> matches
         (map (fn [[p1 p2 :as comp]]
                (let [other-port (other-port port p1 p2)]
                  (conj (all-bridges (disj components [p1 p2]) other-port)
                        comp)))))))

(defn- bridge-max-strength-aux [bridge]
  (let [[p1 p2] (first bridge)
        sum     (+ p1 p2)]
    (if-let [tail (next bridge)]
      (+ sum (apply max (mapv bridge-max-strength-aux tail)))
      sum)))

(defn bridge-max-strength [bridge]
  (bridge-max-strength-aux bridge))

(defn strongest-bridge [component-lines]
  (let [components (set (mapv parse-component component-lines))]
    (->> (all-bridges components 0)
         (map bridge-max-strength)
         (apply max))))

(defn flatten-bridge-aux [prefix [n & ns]]
  (if (seq ns)
    (mapcat (partial flatten-bridge-aux (conj prefix n)) ns)
    [(conj prefix n)]))

(defn flatten-bridge
  "Returns a sequence of component sequences, for the given bridge tree."
  [bridge]
  (flatten-bridge-aux [] bridge))

(defn longest-bridge [component-lines]
  (let [components   (set (mapv parse-component component-lines))
        bridge-trees (all-bridges components 0)
        flat-bridges (mapcat flatten-bridge bridge-trees)
        max-length   (->> flat-bridges
                          (map count)
                          (apply max))]
    (->> flat-bridges
         (filter #(= max-length (count %)))
         (map #(reduce + (flatten %)))
         (apply max))))
