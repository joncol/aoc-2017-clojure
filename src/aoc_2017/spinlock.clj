(ns aoc-2017.spinlock)

(defn spinlock1 [step-size]
  (let [v (->> (range 1 2018)
               (reduce (fn [[v i] n]
                         (let [i*    (mod (+ i step-size) (count v))
                               [l r] (split-at (inc i*) v)]
                           [(vec (concat l [n] r)) (inc i*)]))
                       [[0] 0])
               first)
        i (.indexOf v 2017)]
    (nth v (inc i))))

(defn spinlock2 [^long step-size]
  (let [max (inc (long 50e6))]
    (loop [n 1, i 0, result 0]
      (let [i* (unchecked-remainder-int (unchecked-add i step-size) n)]
        (if (< n max)
          (recur (unchecked-inc n) (unchecked-inc i*) (if (zero? i*) n result))
          result)))))
