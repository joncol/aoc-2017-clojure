(ns aoc-2017.knot-hash
  (:require [aoc-2017.util :refer :all]))

(defn reverse-subarray!
  "Reverses subarray in place."
  [a pos len]
  (doto a
    (arotate! pos)
    (areverse! 0 (dec len))
    (arotate! (- pos))))

(defn knot-hash-reducer [[numbers cur-pos skip-size] len]
  [(reverse-subarray! numbers cur-pos len)
   (mod (+ cur-pos skip-size len) (count numbers))
   (inc skip-size)])

(defn knot-hash1 [n lst]
  (let [knot-lst (first (reduce knot-hash-reducer
                                [(int-array (range n)) 0 0]
                                lst))]
    (* (first knot-lst) (second knot-lst))))

(defn knot-hash2 [s]
  (let [suffix  [17 31 73 47 23]
        lengths (concat (map int s) suffix)
        hash-fn #(reduce knot-hash-reducer % lengths)
        numbers (first (nth (iterate hash-fn [(int-array (range 256)) 0 0]) 64))
        blocks  (partition 16 numbers)]
    (map #(apply bit-xor %) blocks)))

(defn hash->string [hash]
  (->> hash
       (map #(format "%02x" %))
       (apply str)))
