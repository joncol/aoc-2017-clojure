(ns aoc-2017.maze)

(defn- step [[update-fn index instrs]]
  (if (or (neg? index)
          (>= index (count instrs)))
    index
    [update-fn (+ index (nth instrs index)) (update instrs index update-fn)]))

(defn maze [update-fn instrs]
  (->> (iterate step [update-fn 0 (into [] instrs)])
       (take-while sequential?)
       count
       dec))

(defn update-fn-b [x]
  (if (>= x 3)
    (dec x)
    (inc x)))
