(ns aoc-2017.spiral)

;; Only solves part 1
(defn spiral-short [n]
  (let [r (int (Math/sqrt (dec n))), s (if (odd? r) (inc r) r)]
    (+ (Math/abs (- (quot s 2) (mod (dec n) s))) (int (Math/ceil (/ r 2))))))

(defn- move-forward [len dir [x y]]
  (let [dirs    [[1 0] [0 1] [-1 0] [0 -1]]
        [dx dy] (nth dirs dir)]
    (for [i (range 1 (inc len))]
      [(+ x (* i dx)) (+ y (* i dy))])))

(defn spiral-points [pos dir step]
   (let [turn-left #(mod (inc %) 4)
         path1     (move-forward step dir pos)
         new-dir1  (turn-left dir)
         path2     (move-forward step new-dir1 (last path1))
         new-dir2  (turn-left new-dir1)]
     (lazy-seq (concat path1 path2
                       (spiral-points (last path2) new-dir2 (inc step))))))

(defn spiral [n]
  (let [[x y] (nth (cons [0 0] (spiral-points [0 0] 0 1)) (dec n))]
    (+ (Math/abs x) (Math/abs y))))

(defn sum-of-neighbors [m x y]
  (->> (for [yo (range -1 2)]
         (for [xo (range -1 2) :when (not (and (zero? xo) (zero? yo)))]
           (get m [(+ x xo) (+ y yo)] 0)))
       (apply concat)
       (reduce +)))

(defn spiral-sums [lim]
  (let [spiral-sums-aux (fn [m [x y]]
                          (let [sum (sum-of-neighbors m x y)]
                            (if (> sum lim)
                              (reduced sum)
                              (assoc m [x y] sum))))]
    (reduce spiral-sums-aux {[0 0] 1} (spiral-points [0 0] 0 1))))
