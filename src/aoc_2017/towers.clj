(ns aoc-2017.towers
  (:require [clojure.string :as s]
            [clojure.zip :as z]
            [instaparse.core :as insta]))

(def ^:private tower-parser
  (insta/parser "tower    = name <ws> weight children?
                 ws       = #'\\s+'
                 <name>   = #'\\w+'
                 weight   = <'('>#'\\d+'<')'>
                 children = <' -> '> name (<', '> name)*"))

(defn- new-tower
  [towers [_ name [_ weight] [_ & children]]]
  (assoc towers name
         {:name     name
          :weight   (Integer/parseUnsignedInt weight)
          :children children}))

(defn parse-towers [tower-strs]
  (map tower-parser tower-strs))

(defn tower-map [tower-strs]
  (let [parsed-map (reduce new-tower {} (map tower-parser tower-strs))]
    (reduce (fn [acc [name {:keys [weight children]}]]
              (reduce (fn [acc child]
                        (assoc-in acc [child :parent] name))
                      acc
                      children))
            parsed-map
            parsed-map)))

(defn tower-root [tower-map]
  (->> tower-map
       (filter (fn [[_ v]] (not (contains? v :parent))))
       ffirst))

(defn- new-node [node children]
  {:name     (:name node)
   :weight   (:weight node)
   :parent   (:parent node)
   :children children})

(defn tower-zipper-shallow [tower-map]
  (let [towers (map (fn [[k v]] (assoc v :name k)) tower-map)
        g      (group-by :parent towers)]
    (z/zipper :children
              (fn [x] (map #(get tower-map %) (:children x)))
              new-node
              (first (g nil)))))

(defn tree-zipper [root]
  (z/zipper :children :children nil root))

(defn postwalk [f loc]
  (let [loc (if-some [loc (z/down loc)]
              (loop [loc loc]
                (let [loc (postwalk f loc)]
                  (if-some [loc (z/right loc)]
                    (recur loc)
                    (z/up loc))))
              loc)]
    (z/replace loc (f (z/node loc)))))

(defn calc-weights [loc]
  (postwalk (fn [n]
              (assoc n :total-weight
                     (if-not (:children n)
                       (:weight n)
                       (apply + (:weight n)
                              (map :total-weight (:children n))))))
            loc))

(defn tower-zipper [tower-map]
  (-> tower-map
      tower-zipper-shallow
      calc-weights
      z/root
      tree-zipper))

(defn fix-weight [tower-zip]
  (let [nodes       (map (fn [n]
                           {:weight       (:weight n)
                            :total-weight (:total-weight n)
                            :name         (:name n)
                            :parent       (:parent n)})
                         (tree-seq :children :children (z/root tower-zip)))
        by-parent   (group-by :parent nodes)
        one-w       (fn [[k v]] (= 1 (count (group-by :total-weight v))))
        many-ws     (complement one-w)
        one-elem    (fn [[k v]] (= 1 (count v)))
        many-elems  (complement one-elem)
        unbalanced  (second (first (filter many-ws by-parent)))
        g           (group-by :total-weight unbalanced)
        correct-w   (ffirst (filter many-elems g))
        incorrect   (first (filter one-elem g))
        incorrect-w (first incorrect)
        w           (:weight (first (second incorrect)))
        delta       (- correct-w incorrect-w)]
    (+ w delta)))
