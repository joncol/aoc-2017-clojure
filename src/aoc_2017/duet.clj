(ns aoc-2017.duet
  (:require [instaparse.core :as insta])
  (:import [java.util.concurrent LinkedBlockingQueue]))

(def ^:private instruction-parser
  (insta/parser "<stmt> = snd | set | add | mul | mod | rcv | jgz
                 ws     = #'\\s+'
                 reg    = #'\\w'
                 number = #'-?\\d+'
                 <val>  = reg | number
                 snd    = <'snd'> <ws> reg
                 set    = <'set'> <ws> reg <ws> val
                 add    = <'add'> <ws> reg <ws> val
                 mul    = <'mul'> <ws> reg <ws> val
                 mod    = <'mod'> <ws> reg <ws> val
                 rcv    = <'rcv'> <ws> reg
                 jgz    = <'jgz'> <ws> val <ws> val"))

(defn parse-instruction [s]
  (->> (instruction-parser s)
       (insta/transform
        {:reg    #(-> [:reg (keyword %)])
         :number #(-> [:number (Integer/parseInt %)])})
       first))

(defn- get-value [val regs]
  (case (first val)
    :number (second val)
    :reg    (get regs (second val) 0)))

(defn- step-program [instrs instr-strs exe-fn {:keys [ip] :as state}]
  (if (<= 0 ip (dec (count instrs)))
    (let [state* (exe-fn state (get instrs ip))
          jump   (or (:jump state*) 1)]
      (-> state*
          (update :ip #(+ % jump))
          (dissoc :jump)))
    (assoc state :exit true)))

(defn execute [{:keys [regs freq] :as state} instr]
  (case (name (first instr))
    "snd" (let [[_ reg] instr]
            (assoc state :snd-freq (get-value reg regs)))
    "set" (let [[_ [_ reg-name] val] instr]
            (assoc-in state [:regs reg-name] (get-value val regs)))
    "add" (let [[_ [_ reg-name] val] instr]
            (update-in state [:regs reg-name]
                       #(+ (get-value val regs) (or % 0))))
    "mul" (let [[_ [_ reg-name] val] instr]
            (update-in state [:regs reg-name]
                       #(* (get-value val regs) (or % 0))))
    "mod" (let [[_ [_ reg-name] val] instr]
            (update-in state [:regs reg-name]
                       #(mod (or % 0) (get-value val regs))))
    "rcv" (let [[_ [_ reg-name]] instr
                reg-value        (get regs reg-name 0)]
            (if (not (zero? reg-value))
              (assoc state
                     :rcv-freq (:snd-freq state)
                     :exit true)
              state))
    "jgz" (let [[_ cmp jump] instr
                cmp-val      (get-value cmp regs)
                jump-val     (get-value jump regs)]
            (if (pos? cmp-val)
              (assoc state :jump jump-val)
              state))))

(defn duet [instr-strs]
  (let [instrs (vec (map parse-instruction instr-strs))]
    (loop [state {:ip 0}]
      (let [state* (step-program instrs instr-strs execute state)]
        (if (not (:exit state*))
          (recur state*)
          (:rcv-freq state*))))))

(defn- other-program-index [program-index]
  (mod (+ 1 program-index) 2))

(defn execute* [{:keys [regs program-index] :as state} instr]
  (let [other-idx (other-program-index program-index)]
    (case (name (first instr))
      "snd" (let [[_ reg] instr
                  q       (get-in state [:msg-queues other-idx])]
              (.put q (get-value reg regs))
              (update state :snd-count (fnil inc 0)))
      "rcv" (let [[_ [_ reg-name]] instr
                  q                (get-in state [:msg-queues program-index])]
              (if (not (.isEmpty q))
                (let [x (.take q)]
                  (-> state
                      (assoc-in [:regs reg-name] x)
                      (dissoc :waiting)))
                (assoc state
                       :jump 0
                       :waiting true)))
      (execute state instr))))

(defn duet* [instr-strs]
  (let [instrs (vec (map parse-instruction instr-strs))
        qs     [(LinkedBlockingQueue.) (LinkedBlockingQueue.)]]
    (loop [states (for [i (range 2)]
                    {:program-index i
                     :regs          {:p i}
                     :ip            0
                     :msg-queues    qs})]
      (let [states* (vec (for [state states]
                           (step-program instrs instr-strs execute* state)))]
        (if (and (not (every? :exit states*))
                 (not (every? :waiting states*)))
          (recur states*)
          (get (second states*) :snd-count 0))))))
