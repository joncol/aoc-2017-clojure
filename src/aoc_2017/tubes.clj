(ns aoc-2017.tubes)

(def ^:private directions [[1 0] [0 1] [-1 0] [0 -1]])

(defn maze-char [maze-lines x y]
  (get-in maze-lines [y x]))

(defn opposite-dir [dir]
  (case dir
    0 2
    1 3
    2 0
    3 1))

(defn new-dir [maze-lines x y dir]
  (let [dirs      (->> (range 4) (filter #(not= % (opposite-dir dir))))
        offsets   (map #(get directions %) dirs)
        positions (map #(map + [x y] %) offsets)]
    (->> positions
         (map (fn [dir [x y]] [dir x y]) dirs)
         (filter (fn [[dir x y]]
                   (let [ch (maze-char maze-lines x y)]
                     (or (some-> ch Character/isLetter)
                         ((set "|-") ch)))))
         ffirst)))

(defn tubes [maze-lines]
  (loop [x          (.indexOf (first maze-lines) "|")
         y          0
         dir        1
         result     []
         step-count 1]
    (let [[x* y*] (map + [x y] (get directions dir))
          ch      (maze-char maze-lines x* y*)]
      (cond
        (or (= \space ch) (nil? ch)) [(apply str result) step-count]
        (Character/isLetter ch)      (recur x* y* dir (conj result ch)
                                            (inc step-count))
        (#{\- \|} ch)                (recur x* y* dir result (inc step-count))
        (= \+ ch)                    (recur x* y* (new-dir maze-lines x* y* dir)
                                            result (inc step-count))))))
