(ns aoc-2017.defrag
  (:require [aoc-2017.knot-hash :refer [knot-hash2]]
            [aoc-2017.union-find :refer :all]))

(defn hamming-weight [n]
  (loop [c 0, x n]
    (if (pos? x)
      (recur (inc c) (bit-and x (dec x)))
      c)))

(defn create-field [key]
  (->> (for [row (range 128)]
         (str key "-" row))
       (map knot-hash2)
       (map vec)
       vec))

(defn used-count [field]
  (->> field
       (apply concat)
       (map hamming-weight)
       (reduce +)))

(defn- used? [f x y]
  (let [[byte bit] ((juxt quot rem) x 8)]
    (pos? (bit-and (get-in f [y byte]) (bit-shift-left 1 (- 7 bit))))))

(defn group-count [field]
  (let [pos-vec (for [y (range 128), x (range 128) :when (used? field x y)]
                  [x y])
        index-map (zipmap pos-vec (range))]
    (->> index-map
         (reduce (fn [uf [[x y] i]]
                   (as-> uf uf
                     (if (and (< x 127) (used? field (inc x) y))
                       (union uf i (get index-map [(inc x) y]))
                       uf)
                     (if (and (< y 127) (used? field x (inc y)))
                       (union uf i (get index-map [x (inc y)]))
                       uf)))
                 (union-find (count pos-vec)))
         connected-group-count)))
