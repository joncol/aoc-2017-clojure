(ns aoc-2017.scanner
  (:require [instaparse.core :as insta]))

(def scanner-parser (insta/parser "<scanner> = #'\\d+' <': '> #'\\d+'"))

(defn- parse-line [l]
  (let [[depth range] (insta/parse scanner-parser l)]
    (map #(Integer/parseUnsignedInt %) [depth range])))

(defn trip-severity [scanner-lines]
  (->> scanner-lines
       (map parse-line)
       (reduce (fn [acc-severity l]
                 (let [[depth range] l]
                   (if (zero? (mod depth (* 2 (dec range))))
                     (+ acc-severity (* depth range))
                     acc-severity)))
               0)))

(defn- caught? [parsed-lines delay]
  (loop [[[depth range] & tail] parsed-lines]
    (if (zero? (unchecked-remainder-int
                (unchecked-add-int depth delay)
                (bit-shift-left (unchecked-dec-int range) 1)))
      true
      (when (seq tail)
        (recur tail)))))

(defn trip-caught-delay [scanner-lines]
  (let [parsed-lines (map parse-line scanner-lines)]
    (loop [delay 0]
      (if (caught? parsed-lines delay)
        (recur (unchecked-inc-int delay))
        delay))))
