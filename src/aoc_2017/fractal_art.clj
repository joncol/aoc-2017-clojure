(ns aoc-2017.fractal-art
  (:require [clojure.string :as str]
            [instaparse.core :as insta]))

(def ^:private rule-parser
  (insta/parser "<rule>  = pattern <' => '> pattern
                 pattern = #'[.#/]+'"))

(defn parse-rule [s & opts]
  (vec (insta/transform
        {:pattern (fn [s] (vec (str/split s #"\/")))}
        (apply rule-parser s opts))))

(def ^:private initial-pattern
  [".#."
   "..#"
   "###"])

(defn print-pattern [pattern]
  (println (str/join "\n" pattern)))

(defn get-pattern-cell [pattern cell-size i j]
  (let [size  (count pattern)
        xo    (* i cell-size)
        yo    (* j cell-size)
        cells (for [y (range yo (+ yo cell-size))
                    x (range xo (+ xo cell-size))]
                [x y])]
    (->> cells
         (mapv (fn [[x y]]
                 (get-in pattern [y x])))
         (partition cell-size)
         (mapv #(apply str %)))))

(defn subdivide-pattern [pattern]
  (let [size      (count pattern)
        cell-size (if (zero? (bit-and size 1)) 2 3)
        cnt       (quot size cell-size)
        cells     (for [j (range cnt), i (range cnt)] [i j])]
    (->> cells
         (map (partial apply get-pattern-cell pattern cell-size))
         (partition cnt cnt nil)
         vec)))

(defn- rotate-pos [rot size x y]
  (case rot
    0 [x y]
    1 [(- (dec size) y) x]
    2 [(- (dec size) x) (- (dec size) y)]
    3 [y (- (dec size) x)]))

(defn- rotate-pattern [pattern rot]
  (let [size  (count pattern)
        cells (for [j (range size), i (range size)] [i j])]
    (->> cells
         (map (partial apply rotate-pos rot size))
         (map (comp (partial get-in pattern) reverse))
         (partition size)
         (mapv #(apply str %)))))

(defn- flip-pattern [pattern]
  (->> pattern
       (mapv reverse)
       (mapv (partial apply str))))

(defn- get-variations [pattern]
  (->> [pattern (flip-pattern pattern)]
       (mapcat (fn [p] (mapv #(rotate-pattern p %) (range 4))))))

(defn apply-rule [rules pattern]
  (->> pattern
       get-variations
       (map #(get rules %))
       (filter some?)
       first))

(defn join-patterns [patterns]
  (->> patterns
       (mapcat #(apply mapv vector %))
       (mapv #(apply str %))))

(defn pixel-count [pattern]
  (->> (apply str pattern)
       (filter #(= \# %))
       count))

(defn evolve-pattern [n rule-strs]
  (let [apply-rule (memoize apply-rule)
        rules      (into {} (mapv parse-rule rule-strs))]
    (loop [pattern initial-pattern, i 0]
      (let [new (->> (subdivide-pattern pattern)
                     (map (fn [row] (map (fn [p] (apply-rule rules p)) row))))
            p*  (join-patterns new)]
        (if (< (inc i) n)
          (recur p* (inc i))
          p*)))))
