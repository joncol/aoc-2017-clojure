(ns aoc-2017.conflagration
  (:require [instaparse.core :as insta]))

(def ^:private instruction-parser
  (insta/parser "<stmt> = set | sub | mul | jnz
                 ws     = #'\\s+'
                 reg    = #'\\w'
                 number = #'-?\\d+'
                 <val>  = reg | number
                 set    = <'set'> <ws> reg <ws> val
                 sub    = <'sub'> <ws> reg <ws> val
                 mul    = <'mul'> <ws> reg <ws> val
                 jnz    = <'jnz'> <ws> val <ws> val"))

(defn parse-instruction [s]
  (->> (instruction-parser s)
       (insta/transform
        {:reg    #(-> [:reg (keyword %)])
         :number #(-> [:number (Integer/parseInt %)])})
       first))

(defn- get-value [val regs]
  (case (first val)
    :number (second val)
    :reg    (get regs (second val) 0)))

(defn- step-program [instrs instr-strs exe-fn {:keys [ip] :as state}]
  (if (<= 0 ip (dec (count instrs)))
    (let [state* (exe-fn state (get instrs ip))
          jump   (or (:jump state*) 1)]
      (-> state*
          (update :ip #(+ % jump))
          (dissoc :jump)))
    (assoc state :exit true)))

(defn execute [{:keys [regs freq] :as state} instr]
  (case (name (first instr))
    "set" (let [[_ [_ reg-name] val] instr]
            (assoc-in state [:regs reg-name] (get-value val regs)))
    "sub" (let [[_ [_ reg-name] val] instr]
            (update-in state [:regs reg-name]
                       #(- (or % 0) (get-value val regs))))
    "mul" (let [[_ [_ reg-name] val] instr]
            (-> state
                (update :mul-count (fnil inc 0))
                (update-in [:regs reg-name]
                           #(* (get-value val regs) (or % 0)))))
    "jnz" (let [[_ cmp jump] instr
                cmp-val      (get-value cmp regs)
                jump-val     (get-value jump regs)]
            (if (not (zero? cmp-val))
              (assoc state :jump jump-val)
              state))))

(defn mul-count [instr-strs]
  (let [instrs (vec (map parse-instruction instr-strs))]
    (loop [state {:ip 0}]
      (let [state* (step-program instrs instr-strs execute state)]
        (if (not (:exit state*))
          (recur state*)
          (get state* :mul-count 0))))))
