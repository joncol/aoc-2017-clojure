(ns aoc-2017.reallocate
  (:require [aoc-2017.util :refer [rotate]]))

(defn reallocate [banks]
  (let [n     (count banks)
        [t m] (apply max-key second (map-indexed vector (reverse banks)))
        i     (dec (- n t))
        [q r] ((juxt quot rem) m n)
        mask  (take n (concat (repeat r 1) (repeat 0)))
        rot   (rotate (- (inc i)) mask)]
    (into [] (map + (assoc banks i 0) (repeat n q) rot))))

(defn reallocate-until-repetition [banks]
  (let [allocs (iterate reallocate (into [] banks))]
    (reduce (fn [[acc index] a]
              (if-let [i (get acc a)]
                (reduced [(count acc) (- index i)])
                [(assoc acc a index) (inc index)]))
            [nil 0] allocs)))
